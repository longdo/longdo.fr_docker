# longdo.fr_docker

## About

This is my personal page about my career experiences. 

My resume, my blog posts sharing technology knowledge or feedback, my data-driven products developed within my company [DatasGold](https://datasgold.tech) will be mentioned on this website.   

## Technical stacks

This website is built using the following tools:

- [ ] [Wordpress](https://wordpress.com/)

- [ ] [MySQL](https://www.mysql.com/) (will be soon migrated to [MariaDB](https://mariadb.org/))

- [ ] [PHP](https://www.php.net/)

- [ ] [Nginx](https://www.nginx.com/)

- [ ] [Docker](https://www.docker.com/)

## Project status

The website is always under construction.
